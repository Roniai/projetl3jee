<%@page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Visite</title>
<link href="css/bootstrap.min.css" rel="stylesheet" type="text/css" />
</head>
<body>
<%@include file="header.jsp" %>
<p></p>
<div class="container">
	<div class="card">
		<div class="card-header">
		Liste des informations sur les visites des sites touristiques
		</div>
		<div class="card-body">
			<form>
				<button class="btn btn-primary" style="float:right;background:rgb(0,139,139)">
				<a href="ajouterVisiter.do" style="color:white;text-decoration:none">+ Ajouter</a></button>
			</form>
			<table class="table table-striped">
				<tr>
					<th>Numéro visiteur</th>
					<th>Numéro site</th>
					<th>Nombre de jours</th>
					<th>Date de visite</th>
					<th>Edition</th>
					<th>Suppression</th>
				</tr>
				<c:forEach items="${model.visiter}" var="p">
				<tr>
					<td>${p.numvist}</td>
					<td>${p.numsite}</td>
					<td>${p.nbjours}</td>
					<td>${p.datevisite}</td>
					<td><a href="editerVisiter.do?numvist=${p.numvist}&numsite=${p.numsite}"><img src="images/edit.png" width="40" style="margin-bottom:-10px;"></a></td>
					<td><a onclick="return confirm('Etes-vous sûr de supprimer cette information de visite?')" href="supprimerVisiter.do?numvist=${p.numvist}&numsite=${p.numsite}">
					<img src="images/delete.png" width="50" style="margin:-10px 0px"></a></td>
				</tr>
				</c:forEach> 
			</table>
		</div>
	
	</div>
</div>
</body>
</html>
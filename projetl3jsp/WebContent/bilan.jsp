<%@page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Visiter</title>
<link href="css/bootstrap.min.css" rel="stylesheet" type="text/css" />
</head>
<body>
<%@include file="header.jsp" %>
<p></p>
<div class="container">
	<div class="card">
		<div class="card-header">
		 Site touristique :
			<form action="choixNomSite.do" method="get">
				<select name="nomsiteSelect" class="form-control" style="width:200px;display:inline-block">
						<option>${model.nomsiteSelect}</option>
						<option>Tout</option>
					<c:forEach items="${modelS.sites}" var="p">
						<option>${p.nomsite}</option>
					</c:forEach>
				</select>
				<button type="submit" class="btn btn-primary" style="background:rgb(0,139,139);margin-top:-5px">Choisir</button>
			</form>
		</div>
		<div class="card-body">
			<table class="table table-striped">
				<tr>
					<th>Visiteur</th>
					<th>Date de visite</th>
					<th>Tarif journalier</th>
					<th>Nombre de jours</th>
					<th>Montant</th>
				</tr>
				<c:forEach items="${model.bilans}" var="p">
				<tr>
					<td>${p.nomvist}</td>
					<td>${p.datevisite}</td>
					<td>${p.tarifsite}</td>
					<td>${p.nbjours}</td>
					<td>${p.montant}</td>
				</tr>
				</c:forEach>
			</table>
		</div>
	
	</div>
</div>
</body>
</html>
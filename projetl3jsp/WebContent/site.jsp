<!-- TEST COMMIT HOE MIOVA VE -->

<%@page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Site touristique</title>
<link href="css/bootstrap.min.css" rel="stylesheet" type="text/css" />
</head>
<body>
<%@include file="header.jsp" %>
<p></p>
<div class="container">
	<div class="card">
		<div class="card-header">
		Liste des Sites touristiques
		</div>
		<div class="card-body">
			<form>
				<button class="btn btn-primary" style="float:right;background: rgb(0,139,139)">
				<a href="ajouterSite.do" style="color:white;text-decoration:none">+ Ajouter</a></button>
			</form>
			<table class="table table-striped">
				<tr>
					<th>Numéro</th>
					<th>Nom</th>
					<th>Lieu</th>
					<th>Tarif journalier</th>
					<th>Edition</th>
					<th>Suppression</th>
				<!-- 	<th>Bilan</th> -->
				</tr>
				<c:forEach items="${model.sites}" var="p">
				<tr>
					<td>${p.numsite}</td>
					<td>${p.nomsite}</td>
					<td>${p.lieusite}</td>
					<td>${p.tarifsite}</td>
					<td><a href="editerSite.do?numsite=${p.numsite}"><img src="images/edit.png" width="40" style="margin-bottom:-10px;"></a></td>
					<td><a onclick="return confirm('Etes-vous sûr de supprimer ce site?')" href="supprimerSite.do?numsite=${p.numsite}">
					<img src="images/delete.png" width="50" style="margin:-10px 0px"></a></td>
					<%-- <td><a href="bilan.do?nomsite=${p.nomsite}&numsite=${p.numsite}"><img src="images/chart.png" width="50" style="margin:-10px 0px"></a></td --%>
				</tr>
				</c:forEach> 
			</table>
		</div>
	
	</div>
</div>
</body>
</html>
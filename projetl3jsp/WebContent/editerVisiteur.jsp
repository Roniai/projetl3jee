<%@page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<!DOCTYPE html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset="UTF-8">
<title>Formulaire d'edition</title>
<link href="css/bootstrap.min.css" rel="stylesheet" type="text/css" />
</head>
<body>
<%@include file="header.jsp" %>
<p></p>
<div class="container" style="width:500px">
	<div class="card">
		<div class="card-header">
		Modification du visiteur
		</div>
		<div class="card-body">
			<form action="updateVisiteur.do" method="post">
			<div class="form-group">
				<label class="control-label">Numéro : </label>
				<input type="text" name="numvist" class="form-control" value="${visiteur.numvist}" autocomplete="off"/>
				<input type="hidden" name="numvist_hidden" class="form-control" value="${visiteur.numvist}"/>
			</div>
			<div class="form-group">
				<label class="control-label">Nom : </label>
				<input type="text" name="nomvist" class="form-control" value="${visiteur.nomvist}" autocomplete="off"/>
			</div>	
			<div class="form-group">
				<label class="control-label">Adresse : </label>
				<input type="text" name="adrvist" class="form-control" value="${visiteur.adrvist}" autocomplete="off"/>
			</div>	
			<div>
				<button type="submit" class="btn btn-primary" style="background: rgb(0,139,139)">Modifier</button>
			</div>		
			</form>
		</div>
	
	</div>
</div>
</body>
</html>
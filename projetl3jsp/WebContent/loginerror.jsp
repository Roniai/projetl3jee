<%@page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>Error</title>
</head>
<body>
	<div style="
		  	position: absolute;
          	left: 50%;
          	top: 50%;
          	-webkit-transform: translate(-50%, -50%);
          	transform: translate(-50%, -50%);">
        <img src="images/error.png" width="180px" style="margin-left:50px">
        <p style="color:red;font-size:20pt;font-weight:bolder;text-align:center">Erreur d'authentification</p>
        <a href="index.jsp">Essayer à nouveau</a>
        </div>
</body>
</html>
package metier.table;

public class Login {
	private String nomuser;
	private String mdpuser;
	private int connected;
	
	public String getNomuser() {
		return nomuser;
	}
	public void setNomuser(String nomuser) {
		this.nomuser = nomuser;
	}
	public String getMdpuser() {
		return mdpuser;
	}
	public void setMdpuser(String mdpuser) {
		this.mdpuser = mdpuser;
	}
	public int getConnected() {
		return connected;
	}
	public void setConnected(int connected) {
		this.connected = connected;
	}
}

//MODELE (JavaBeans)

package metier.table;

import java.io.Serializable;

public class Visiteur implements Serializable{
	private String numvist;
	private String numvist_hidden;
	private String nomvist;
	private String adrvist;
	
	public Visiteur(){
		super();
	}
	
	public Visiteur(String numvist, String nomvist, String adrvist){
		super();
		this.numvist = numvist;
		this.nomvist = nomvist;
		this.adrvist = adrvist;
		this.numvist_hidden = "";	
	}

	public String getNumvist() {
		return numvist;
	}

	public void setNumvist(String numvist) {
		this.numvist = numvist;
	}

	public String getNomvist() {
		return nomvist;
	}

	public void setNomvist(String nomvist) {
		this.nomvist = nomvist;
	}

	public String getAdrvist() {
		return adrvist;
	}

	public void setAdrvist(String adrvist) {
		this.adrvist = adrvist;
	}

	@Override
	public String toString() {
		return "Visiteur [numvist=" + numvist + ", nomvist=" + nomvist
				+ ", adrvist=" + adrvist + "]";
	}

	public String getNumvist_hidden() {
		return numvist_hidden;
	}

	public void setNumvist_hidden(String numvist_hidden) {
		this.numvist_hidden = numvist_hidden;
	}
	
	
}

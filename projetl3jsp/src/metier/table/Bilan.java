package metier.table;

public class Bilan {
	private String nomvist;
	private int nbjours;
	private String datevisite;
	private int tarifsite;
	private int montant;
	private String nomsite;
	
	public Bilan() {
		super();
	}
	
	public Bilan(String nomvist, int nbjours, String datevisite, int tarifsite, int montant) {
		super();
		this.nomvist = nomvist;
		this.nbjours = nbjours;
		this.datevisite = datevisite;
		this.tarifsite = tarifsite;
		this.montant = montant;
	}
	public String getNomvist() {
		return nomvist;
	}
	public void setNomvist(String nomvist) {
		this.nomvist = nomvist;
	}
	public int getNbjours() {
		return nbjours;
	}
	public void setNbjours(int nbjours) {
		this.nbjours = nbjours;
	}
	public String getDatevisite() {
		return datevisite;
	}
	public void setDatevisite(String i) {
		this.datevisite = i;
	}
	public int getTarifsite() {
		return tarifsite;
	}
	public void setTarifsite(int tarifsite) {
		this.tarifsite = tarifsite;
	}
	public int getMontant() {
		return montant;
	}
	public void setMontant(int montant) {
		this.montant = montant;
	}

	public String getNomsite() {
		return nomsite;
	}

	public void setNomsite(String nomsite) {
		this.nomsite = nomsite;
	}
	
}

package metier.table;

public class Visiter {
	private String numvist;
	private String numsite;
	private String numvist_hidden;
	private String numsite_hidden;
	private int nbjours;
	private String datevisite;
	
	public Visiter() {
		super();
	}
	public Visiter(String numvist, String numsite, int nbjours, String datevisite) {
		super();
		this.numvist = numvist;
		this.numsite = numsite;
		this.numvist_hidden = "";
		this.numsite_hidden = "";
		this.nbjours = nbjours;
		this.datevisite = datevisite;
	}
	
	public String getNumvist() {
		return numvist;
	}
	public void setNumvist(String numvist) {
		this.numvist = numvist;
	}
	public String getNumsite() {
		return numsite;
	}
	public void setNumsite(String numsite) {
		this.numsite = numsite;
	}
	public int getNbjours() {
		return nbjours;
	}
	public void setNbjours(int nbjours) {
		this.nbjours = nbjours;
	}
	public String getDatevisite() {
		return datevisite;
	}
	public void setDatevisite(String datevisite) {
		this.datevisite = datevisite;
	}
	public String getNumvist_hidden() {
		return numvist_hidden;
	}
	public void setNumvist_hidden(String numvist_hidden) {
		this.numvist_hidden = numvist_hidden;
	}
	public String getNumsite_hidden() {
		return numsite_hidden;
	}
	public void setNumsite_hidden(String numsite_hidden) {
		this.numsite_hidden = numsite_hidden;
	}
	
	
}

package metier.table;

public class Site {
	private String numsite;
	private String numsite_hidden;
	private String nomsite;
	private String lieusite;
	private int tarifsite;
	
	public Site(){
		super();
	}
	public Site(String numsite, String nomsite, String lieusite, int tarifsite) {
		super();
		this.numsite = numsite;
		this.nomsite = nomsite;
		this.lieusite = lieusite;
		this.tarifsite = tarifsite;
		this.numsite_hidden = "";
	}
	public String getNumsite() {
		return numsite;
	}
	public void setNumsite(String numsite) {
		this.numsite = numsite;
	}
	public String getNomsite() {
		return nomsite;
	}
	public void setNomsite(String nomsite) {
		this.nomsite = nomsite;
	}
	public String getLieusite() {
		return lieusite;
	}
	public void setLieusite(String lieusite) {
		this.lieusite = lieusite;
	}
	public int getTarifsite() {
		return tarifsite;
	}
	public void setTarifsite(int tarifsite) {
		this.tarifsite = tarifsite;
	}
	public String getNumsite_hidden() {
		return numsite_hidden;
	}
	public void setNumsite_hidden(String numsite_hidden) {
		this.numsite_hidden = numsite_hidden;
	}
	
}

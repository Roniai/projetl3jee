package dao;

import java.util.List;

import metier.table.Site;

public interface IntSiteDao {
	public Site save(Site s);
	public List<Site> afficherSite();
	public Site getSite(String num);
	public Site updateSite(Site s);
	public void deleteSite(String num);
	public Site getToutNomSite();
}

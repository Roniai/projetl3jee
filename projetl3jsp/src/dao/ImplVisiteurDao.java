package dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import metier.SingletonConnection;
import metier.table.Visiteur;

public class ImplVisiteurDao implements IntVisiteurDao{

	@Override
	public Visiteur save(Visiteur v) {
		Connection conn = SingletonConnection.getConnection();
		try{
			PreparedStatement ps = conn.prepareStatement("INSERT INTO visiteur(numvist,nomvist,adrvist) VALUES(?,?,?)");
			ps.setString(1, v.getNumvist());
			ps.setString(2, v.getNomvist());
			ps.setString(3, v.getAdrvist());
			ps.executeUpdate();
			ps.close();
		}
		catch(SQLException e){
			e.printStackTrace();
		}
		return v;
	}
	
	//Recherche par MC = Mot Cl�
	@Override
	public List<Visiteur> visiteurParMC(String mc) {
		List<Visiteur> vist = new ArrayList<Visiteur>();
		Connection conn = SingletonConnection.getConnection();
		try{
			PreparedStatement ps = conn.prepareStatement("SELECT * FROM visiteur WHERE numvist LIKE ? OR nomvist LIKE ? ORDER BY numvist ASC");
			ps.setString(1, "%"+mc+"%");
			ps.setString(2, "%"+mc+"%");
			ResultSet rs = ps.executeQuery();
			while(rs.next()){
				Visiteur v = new Visiteur();
				v.setNumvist(rs.getString("numvist"));
				v.setNomvist(rs.getString("nomvist"));
				v.setAdrvist(rs.getString("adrvist"));
				vist.add(v);
			}
			
		}
		catch(SQLException e){
			e.printStackTrace();
		}
		return vist;
	}

	@Override
	public Visiteur getVisiteur(String num) {
		Connection conn = SingletonConnection.getConnection();
		Visiteur v = new Visiteur();
		try{
			PreparedStatement ps = conn.prepareStatement("SELECT * FROM visiteur WHERE numvist = ?");
			ps.setString(1, num);
			ResultSet rs = ps.executeQuery(); //executeQuery => ho an'ny type SELECT
			if(rs.next()){
				v.setNumvist(rs.getString("numvist"));
				v.setNomvist(rs.getString("nomvist"));
				v.setAdrvist(rs.getString("adrvist"));
			}	
		}
		catch(SQLException e){
			e.printStackTrace();
		}
		
		return v;
	}

	@Override
	public Visiteur updateVisiteur(Visiteur v) {
		Connection conn = SingletonConnection.getConnection();
		try{
			PreparedStatement ps = conn.prepareStatement("UPDATE visiteur SET numvist=?,nomvist=?,adrvist=? WHERE numvist=?");
			ps.setString(1, v.getNumvist());
			ps.setString(2, v.getNomvist());
			ps.setString(3, v.getAdrvist());
			ps.setString(4, v.getNumvist_hidden());
			ps.executeUpdate(); //executeUpdate => ho an'ny type DELETE,UPDATE,INSERT
			ps.close();
		}
		catch(SQLException e){
			e.printStackTrace();
		}
		return v;
	}

	@Override
	public void deleteVisiteur(String num) {
		Connection conn = SingletonConnection.getConnection();
		try{
			PreparedStatement ps = conn.prepareStatement("DELETE FROM visiteur WHERE numvist=?");
			ps.setString(1, num);
			ps.executeUpdate();
			ps.close();
		}
		catch(SQLException e){
			e.printStackTrace();
		}	
	}
	
}

package dao;

import java.util.List;

import metier.table.Visiteur;

public interface IntVisiteurDao {
	public Visiteur save(Visiteur v);
	public List<Visiteur> visiteurParMC(String mc);
	public Visiteur getVisiteur(String num);
	public Visiteur updateVisiteur(Visiteur v);
	public void deleteVisiteur(String num);
}

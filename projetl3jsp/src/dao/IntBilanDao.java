package dao;

import java.util.List;

import metier.table.Bilan;

public interface IntBilanDao {
	public List<Bilan> bilanVisiter(String nomsiteSelect);
}

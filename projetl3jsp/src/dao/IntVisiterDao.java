package dao;

import java.util.List;

import metier.table.Bilan;
import metier.table.Visiter;

public interface IntVisiterDao {
	
	//Table Visiter
	
	public Visiter save(Visiter v);
	public List<Visiter> afficherVisiter();
	public Visiter getVisiter(String numvist,String numsite);
	public Visiter updateVisiter(Visiter v);
	public void deleteVisiter(String numvist,String numsite);
	
	//Pour Bilan (requete jointure)
	
	//public List<Bilan> bilanVisiter(String numsite);
	
}

package dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import metier.SingletonConnection;
import metier.table.Bilan;

public class ImplBilanDao implements IntBilanDao{
	
	//Pour Bilan (requ�te jointure)
	
	@Override
	public List<Bilan> bilanVisiter(String nomsiteSelect) {
		List<Bilan> bilan = new ArrayList<Bilan>();
		Connection conn = SingletonConnection.getConnection();
		try{
			if(nomsiteSelect.equals("") || nomsiteSelect.equals("Tout")) {
				PreparedStatement ps = conn.prepareStatement(""
						+ "SELECT visiteur.nomvist,visiter.datevisite,site.tarifsite,visiter.nbjours,site.tarifsite*visiter.nbjours as montant "
						+ "FROM visiteur,visiter,site "
						+ "WHERE visiteur.numvist=visiter.numvist AND site.numsite=visiter.numsite");
				ResultSet rs = ps.executeQuery();
				while(rs.next()){
					Bilan b = new Bilan();
					b.setNomvist(rs.getString("nomvist"));
					b.setDatevisite(rs.getString("datevisite"));
					b.setTarifsite(rs.getInt("tarifsite"));
					b.setNbjours(rs.getInt("nbjours"));
					b.setMontant(rs.getInt("montant"));
					bilan.add(b);
				}
			}
			else {
				PreparedStatement ps = conn.prepareStatement(""
						+ "SELECT visiteur.nomvist,visiter.datevisite,site.tarifsite,visiter.nbjours,site.tarifsite*visiter.nbjours as montant "
						+ "FROM visiteur,visiter,site "
						+ "WHERE visiteur.numvist=visiter.numvist AND site.numsite=visiter.numsite AND site.nomsite=?");
				ps.setString(1, nomsiteSelect);
				ResultSet rs = ps.executeQuery();
				while(rs.next()){
					Bilan b = new Bilan();
					b.setNomvist(rs.getString("nomvist"));
					b.setDatevisite(rs.getString("datevisite"));
					b.setTarifsite(rs.getInt("tarifsite"));
					b.setNbjours(rs.getInt("nbjours"));
					b.setMontant(rs.getInt("montant"));
					bilan.add(b);
				}
			}
			
		}
		catch(SQLException e){
			e.printStackTrace();
		}
		return bilan;
	}
	
}

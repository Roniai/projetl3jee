package dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import metier.SingletonConnection;
import metier.table.Bilan;
import metier.table.Visiter;

public class ImplVisiterDao implements IntVisiterDao{
	
	//Table Visiter

	@Override
	public Visiter save(Visiter v) {
		Connection conn = SingletonConnection.getConnection();
		try{
			PreparedStatement ps = conn.prepareStatement("INSERT INTO visiter(numvist,numsite,nbjours,datevisite) VALUES(?,?,?,?)");
			ps.setString(1, v.getNumvist());
			ps.setString(2, v.getNumsite());
			ps.setInt(3, v.getNbjours());
			ps.setString(4, v.getDatevisite());
			ps.executeUpdate();
			ps.close();
		}
		catch(SQLException e){
			e.printStackTrace();
		}
		return v;
	}

	@Override
	public List<Visiter> afficherVisiter() {
		List<Visiter> vist = new ArrayList<Visiter>();
		Connection conn = SingletonConnection.getConnection();
		try{
			PreparedStatement ps = conn.prepareStatement("SELECT * FROM visiter ORDER BY numvist ASC");
			ResultSet rs = ps.executeQuery();
			while(rs.next()){
				Visiter v = new Visiter();
				v.setNumvist(rs.getString("numvist"));
				v.setNumsite(rs.getString("numsite"));
				v.setNbjours(rs.getInt("nbjours"));
				v.setDatevisite(rs.getString("datevisite"));
				vist.add(v);
			}
			
		}
		catch(SQLException e){
			e.printStackTrace();
		}
		return vist;
	}

	@Override
	public Visiter getVisiter(String numvist, String numsite) {
		Connection conn = SingletonConnection.getConnection();
		Visiter v = new Visiter();
		try{
			PreparedStatement ps = conn.prepareStatement("SELECT * FROM visiter WHERE numvist=? AND numsite=?");
			ps.setString(1, numvist);
			ps.setString(2, numsite);
			ResultSet rs = ps.executeQuery(); //executeQuery => ho an'ny type SELECT
			if(rs.next()){
				v.setNumvist(rs.getString("numvist"));
				v.setNumsite(rs.getString("numsite"));
				v.setNbjours(rs.getInt("nbjours"));
				v.setDatevisite(rs.getString("datevisite"));
			}	
		}
		catch(SQLException e){
			e.printStackTrace();
		}
		
		return v;
	}

	@Override
	public Visiter updateVisiter(Visiter v) {
		Connection conn = SingletonConnection.getConnection();
		try{
			PreparedStatement ps = conn.prepareStatement("UPDATE visiter SET numvist=?,numsite=?,nbjours=?,datevisite=? WHERE numvist=? AND numsite=?");
			ps.setString(1, v.getNumvist());
			ps.setString(2, v.getNumsite());
			ps.setInt(3, v.getNbjours());
			ps.setString(4, v.getDatevisite());
			ps.setString(5, v.getNumvist_hidden());
			ps.setString(6, v.getNumsite_hidden());
			ps.executeUpdate(); //executeUpdate => ho an'ny type DELETE,UPDATE,INSERT
			ps.close();
		}
		catch(SQLException e){
			e.printStackTrace();
		}
		return v;
	}

	@Override
	public void deleteVisiter(String numvist, String numsite) {
		Connection conn = SingletonConnection.getConnection();
		try{
			PreparedStatement ps = conn.prepareStatement("DELETE FROM visiter WHERE numvist=? AND numsite=?");
			ps.setString(1, numvist);
			ps.setString(2, numsite);
			ps.executeUpdate();
			ps.close();
		}
		catch(SQLException e){
			e.printStackTrace();
		}	
		
	}

	
//	@Override
//	public List<Bilan> bilanVisiter(String numsite) {
//		List<Bilan> bilan = new ArrayList<Bilan>();
//		Connection conn = SingletonConnection.getConnection();
//		try{
//			PreparedStatement ps = conn.prepareStatement(""
//				+ "SELECT visiteur.nomvist,visiter.datevisite,site.tarifsite,visiter.nbjours,site.tarifsite*visiter.nbjours as montant "
//				+ "FROM visiteur,visiter,site "
//				+ "WHERE visiteur.numvist=visiter.numvist AND site.numsite=visiter.numsite AND site.numsite=?"); //AND site.numsite=?
//			//SELECT visiteur.nomvist,visiter.datevisite,site.tarifsite,visiter.nbjours,site.tarifsite*visiter.nbjours as montant FROM visiteur,visiter,site WHERE visiteur.numvist=visiter.numvist AND site.numsite=visiter.numsite AND site.numsite="E32";
//			ps.setString(1, numsite);
//			ResultSet rs = ps.executeQuery();
//			while(rs.next()){
//				Bilan b = new Bilan();
//				b.setNomvist(rs.getString("nomvist"));
//				b.setDatevisite(rs.getString("datevisite"));
//				b.setTarifsite(rs.getInt("tarifsite"));
//				b.setNbjours(rs.getInt("nbjours"));
//				b.setMontant(rs.getInt("montant"));
//				bilan.add(b);
//			}
//		}
//		catch(SQLException e){
//			e.printStackTrace();
//		}
//		return bilan;
//	}

}

package dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import metier.SingletonConnection;
import metier.table.Site;

public class ImplSiteDao implements IntSiteDao {

	@Override
	public Site save(Site s) {
		Connection conn = SingletonConnection.getConnection();
		try{
			PreparedStatement ps = conn.prepareStatement("INSERT INTO site(numsite,nomsite,lieusite,tarifsite) VALUES(?,?,?,?)");
			ps.setString(1, s.getNumsite());
			ps.setString(2, s.getNomsite());
			ps.setString(3, s.getLieusite());
			ps.setInt(4, s.getTarifsite());
			ps.executeUpdate();
			ps.close();
		}
		catch(SQLException e){
			e.printStackTrace();
		}
		return s;
	}

	@Override
	public List<Site> afficherSite() {
		List<Site> site = new ArrayList<Site>();
		Connection conn = SingletonConnection.getConnection();
		try{
			PreparedStatement ps = conn.prepareStatement("SELECT * FROM site ORDER BY numsite ASC");
			ResultSet rs = ps.executeQuery();
			while(rs.next()){
				Site s = new Site();
				s.setNumsite(rs.getString("numsite"));
				s.setNomsite(rs.getString("nomsite"));
				s.setLieusite(rs.getString("lieusite"));
				s.setTarifsite(rs.getInt("tarifsite"));
				site.add(s);
			}
			
		}
		catch(SQLException e){
			e.printStackTrace();
		}
		return site;
	}
	
	@Override
	public Site getSite(String num) {
		Connection conn = SingletonConnection.getConnection();
		Site s = new Site();
		try{
			PreparedStatement ps = conn.prepareStatement("SELECT * FROM site WHERE numsite = ?");
			ps.setString(1, num);
			ResultSet rs = ps.executeQuery(); //executeQuery => ho an'ny type SELECT
			if(rs.next()){
				s.setNumsite(rs.getString("numsite"));
				s.setNomsite(rs.getString("nomsite"));
				s.setLieusite(rs.getString("lieusite"));
				s.setTarifsite(rs.getInt("tarifsite"));
			}	
		}
		catch(SQLException e){
			e.printStackTrace();
		}
		
		return s;
	}

	@Override
	public Site updateSite(Site s) {
		Connection conn = SingletonConnection.getConnection();
		try{
			PreparedStatement ps = conn.prepareStatement("UPDATE site SET numsite=?,nomsite=?,lieusite=?,tarifsite=? WHERE numsite=?");
			ps.setString(1, s.getNumsite());
			ps.setString(2, s.getNomsite());
			ps.setString(3, s.getLieusite());
			ps.setInt(4, s.getTarifsite());
			ps.setString(5, s.getNumsite_hidden());
			ps.executeUpdate(); //executeUpdate => ho an'ny type DELETE,UPDATE,INSERT
			ps.close();
		}
		catch(SQLException e){
			e.printStackTrace();
		}
		return s;
	}

	@Override
	public void deleteSite(String num) {
		Connection conn = SingletonConnection.getConnection();
		try{
			PreparedStatement ps = conn.prepareStatement("DELETE FROM site WHERE numsite=?");
			ps.setString(1, num);
			ps.executeUpdate();
			ps.close();
		}
		catch(SQLException e){
			e.printStackTrace();
		}	
		
	}

	@Override
	public Site getToutNomSite() {
		Connection conn = SingletonConnection.getConnection();
		Site s = new Site();
		try{
			PreparedStatement ps = conn.prepareStatement("SELECT * FROM site");
			ResultSet rs = ps.executeQuery(); //executeQuery => ho an'ny type SELECT
			if(rs.next()){
				s.setNomsite(rs.getString("nomsite"));
			}	
		}
		catch(SQLException e){
			e.printStackTrace();
		}
		
		return s;
	}

}

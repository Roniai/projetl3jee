package dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import metier.SingletonConnection;
import metier.table.Login;

public class LoginDao {
	public int authentification(Login g) {
		Connection conn = SingletonConnection.getConnection();
		int trouve=0;
		try{
			PreparedStatement ps = conn.prepareStatement("SELECT * FROM login");
			ResultSet rs = ps.executeQuery(); //executeQuery => ho an'ny type SELECT
			if(rs.next()){
				if(rs.getString("nomuser").equals(g.getNomuser())) {
					if(rs.getString("mdpuser").equals(g.getMdpuser())) {
						trouve=1;
					}	
				}
			}	
		}
		catch(SQLException e){
			e.printStackTrace();
		}
		return trouve;
	}
}

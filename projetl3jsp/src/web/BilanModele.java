package web;

import java.util.ArrayList;
import java.util.List;

import metier.table.Bilan;

public class BilanModele {
	private String nomsiteSelect;
	List<Bilan> bilans = new ArrayList<>();

	public List<Bilan> getBilans() {
		return bilans;
	}

	public void setBilans(List<Bilan> bilans) {
		this.bilans = bilans;
	}

	public String getNomsiteSelect() {
		return nomsiteSelect;
	}

	public void setNomsiteSelect(String nomsiteSelect) {
		this.nomsiteSelect = nomsiteSelect;
	}
	
}

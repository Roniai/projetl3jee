package web;

import java.util.ArrayList;
import java.util.List;

import metier.table.Site;

public class SiteModele {
	List<Site> sites = new ArrayList<>();

	public List<Site> getSites() {
		return sites;
	}

	public void setSites(List<Site> sites) {
		this.sites = sites;
	}
	
}

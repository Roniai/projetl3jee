package web;

import java.util.ArrayList;
import java.util.List;

import metier.table.Visiter;

public class VisiterModele {
	List<Visiter> visiter = new ArrayList<>();

	public List<Visiter> getVisiter() {
		return visiter;
	}

	public void setVisiter(List<Visiter> visiter) {
		this.visiter = visiter;
	}
	
}

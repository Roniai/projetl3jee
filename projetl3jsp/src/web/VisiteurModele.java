package web;

import java.util.ArrayList;
import java.util.List;

import metier.table.Visiteur;

public class VisiteurModele {
	private String motCle;
	List<Visiteur> visiteurs = new ArrayList<>();
	
	public String getMotCle(){
		return motCle;
	}
	public void setMotCle(String motCle){
		this.motCle = motCle;
	}
	public List<Visiteur> getVisiteurs(){
		return visiteurs;
	}
	public void setVisiteurs(List<Visiteur> visiteurs){
		this.visiteurs = visiteurs;
	}
	
}
